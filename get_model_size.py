# tool to get size of a model whose parameters you provide
# this should be helpful in figuring out the hyperparameters to approximate the models you'll compare with
import argparse
import jax
import jax_tokenizer as tokenizer
import amlpstack
import gmlpstack
from jax import random

parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "--seq_len",
    dest="seq_len",
    type=int,
    required=True,
    help="length of input sequence",
)
parser.add_argument(
    "--stack_len",
    dest="stack_len",
    type=int,
    required=True,
    help="number of amlp blocks you want stacked in the architecture",
)
parser.add_argument(
    "--hidden_dim",
    dest="d_Z",
    type=int,
    required=True,
    help="out dimension of first projection in a gMLP / aMLP block",
)
parser.add_argument(
    "--architecture",
    dest="architecture",
    type=str,
    required=True,
    help="choose between the use of gMLP and aMLP blocks",
)
parser.add_argument(
    "--encoding_len",
    dest="encoding_len",
    type=int,
    required=False,
    default=256,
    help="vocab length",
)


if __name__ == "__main__":
    args = parser.parse_args()
    rng = random.PRNGKey(0)
    if args.architecture == "aMLP":
        init_model, apply_model = amlpstack.AMLPStack(
            args.seq_len,
            args.d_Z,
            args.stack_len,
        )
    elif args.architecture == "gMLP":
        init_model, apply_model = gmlpstack.GMLPStack(
            args.seq_len,
            args.d_Z,
            args.stack_len,
        )
    else:
        raise Exception("unknown architecture")
    key, rng = random.split(rng)
    _, params, _ = init_model(key, (args.seq_len, args.encoding_len))
    flattened_params, _ = jax.tree_util.tree_flatten(params)
    total = sum([x.size for x in flattened_params])
    print(total)
    print(str(total / 1e3) + "K")
    print(str(total / 1e6) + "M")
