#!/usr/bin/env python3
"""
Minimalist and sane interface with the PEP8 breaking (and non idempotent) logging STL module
"""
import logging
import os
import datetime
import functools
import sys

LOG_DIR = os.environ.get("LOG_DIR") or "./"

filename = "/log_" + str(datetime.datetime.now()) + ".tsv"

log_format = "%(asctime)s\t%(levelname)s\t%(message)s"

logging.basicConfig(
    level=logging.DEBUG,
    filename=LOG_DIR + filename,
    format=log_format,
)


def info(string):
    print(string)
    logging.info(string)


# workaround because guildai sets logging level higher than debug making it
# harder to log this
debug = logging.debug

info("New run initiated")

step = 0


def scalars(scalars_dict):
    """
    Log scalars for guildai / tensorboard
    """
    global step
    assert step is not None
    info(f"step: {step}")
    step = step + 1
    for scalar in scalars_dict:
        info(f"{scalar}: {scalars_dict[scalar]}")


def scalars_noinc(scalars_dict):
    """
    Log scalars for guildai / tensorboard
    Do not increment the step
    """
    global step
    assert step is not None
    info(f"step: {step}")
    for scalar in scalars_dict:
        info(f"{scalar}: {scalars_dict[scalar]}")


def iswrapped(function):
    return function.__dict__.get("__wrapped__") is not None


def out(function):
    if iswrapped(function):

        @functools.wraps(function)
        def wrapped_fun(*args, **kwargs):
            out = function(*args, **kwargs)
            debug(
                f"{function.__wrapped__.__code__.co_filename} [{function.__wrapped__.__code__.co_firstlineno}] {function.__wrapped__.__name__} = {out}"
            )
            return out

    else:

        @functools.wraps(function)
        def wrapped_fun(*args, **kwargs):
            out = function(*args, **kwargs)
            debug(
                f"{function.__code__.co_filename} [{function.__code__.co_firstlineno}] {function.__name__} = {out}"
            )
            return out

    return wrapped_fun


def prop(property_fun):
    def decorator(function):
        @functools.wraps(function)
        def wrapped_fun(*args, **kwargs):
            out = function(*args, **kwargs)
            debug(
                f"{property_fun.__module__}.{property_fun.__name__}({function.__code__.co_filename} [{function.__code__.co_firstlineno}] {function.__name__}(...)) == {property_fun(out)}"
            )
            return out

        return wrapped_fun

    return decorator


def yield_prop(property_fun):
    def decorator(function):
        @functools.wraps(function)
        def wrapped_fun(*args, **kwargs):
            out = function(*args, **kwargs)
            debug(
                f"{property_fun.__module__}.{property_fun.__name__}({function.__code__.co_filename} [{function.__code__.co_firstlineno}] {function.__name__}(...)) == {property_fun(out)}"
            )
            yield out

        return wrapped_fun

    return decorator


def yield_out(function):
    @functools.wraps(function)
    def wrapped_fun(*args, **kwargs):
        for out in function(*args, **kwargs):
            debug(
                f"{function.__code__.co_filename} [{function.__code__.co_firstlineno}] {function.__name__} = {out}"
            )
            yield out

    return wrapped_fun


def log_command_run():
    debug(f"command run: {' '.join(sys.argv)}")


def log_env_vars():
    for key in os.environ:
        debug(f"[env var] {key}={os.environ[key]}")


log_command_run()
log_env_vars()
