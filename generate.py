# generate text using saved model
import argparse
import pickle
import jax
from jax import random
import amlpstack
import gmlpstack
import jax_tokenizer as tokenizer
import sampling
import jaxlib
import jax.numpy as jnp
import functools

parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "--model_path",
    dest="model_path",
    metavar="path",
    type=str,
    required=True,
    help="path to the saved model as a pickle file",
)
parser.add_argument("--seed", dest="seed", type=int, default=0, help="PRNG seed")
parser.add_argument(
    "--prompt",
    dest="prompt",
    type=str,
    default="Hello World!",
    help="prompt to the saved model",
)
parser.add_argument(
    "--architecture",
    dest="architecture",
    type=str,
    required=True,
    help="choose between the use of gMLP and aMLP blocks",
)
parser.add_argument(
    "--out_len",
    dest="out_len",
    type=int,
    default=5,
    help="length of output generation",
)


# @functools.partial(jax.jit, static_argnums=(2, 4))
def generate_until(
    rng,
    params,
    applys,
    prompt: jaxlib.xla_extension.DeviceArray,
    apply_model,
    seq_len: int,
    out_len=10,
    top_k=50,
    top_p=0.95,
    temp=1,
):
    continuation = jnp.array([-1])
    while len(continuation) < out_len:
        padded_prompt = tokenizer.pad_right(prompt, seq_len)
        # padded_prompt.shape: (seq_len, )
        all_logits = apply_model(params, applys, tokenizer.magnify(padded_prompt))
        # all_logits.shape: (seq_len,)
        # take the logit after the original prompt len
        logits = all_logits[len(prompt) - 1]
        # logits.shape: (,)
        key, rng = random.split(rng)
        # TODO check sample function
        token = jnp.array([sampling.sample(key, logits, top_k, top_p, temp)])
        if jnp.all(continuation == jnp.array([-1])):
            continuation = token
        else:
            continuation = jnp.concatenate([continuation, token])
        prompt = jnp.concatenate([prompt, token])
        # clip any overflow from the left to ensure that prompt is of size seq_len
        prompt = prompt[len(prompt) - seq_len :]
    return continuation


def load_model(model, architecture):
    if args.architecture == "aMLP":
        init_model, apply_model = amlpstack.AMLPStack(
            model["seq_len"],
            model["d_Z"],
            model["stack_len"],
        )
    elif args.architecture == "gMLP":
        init_model, apply_model = gmlpstack.GMLPStack(
            model["seq_len"],
            model["d_Z"],
            model["stack_len"],
        )
    else:
        raise Exception("unknown architecture")
    # use fixed seed
    rng = random.PRNGKey(0)
    key, rng = random.split(rng)
    _, _, applys = init_model(key, (model["seq_len"], 256))
    params = model["params"]
    return apply_model, applys, params


if __name__ == "__main__":
    args = parser.parse_args()
    rng = random.PRNGKey(args.seed)
    with open(args.model_path, "rb") as file:
        model = pickle.load(file)
    apply_model, applys, params = load_model(model, args.architecture)

    tokenized_prompt = tokenizer.tokenize(args.prompt)
    # tokenized_prompt.shape: (seq_len, )
    key, rng = random.split(rng)
    output = generate_until(
        key,
        params,
        applys,
        tokenized_prompt,
        apply_model,
        model["seq_len"],
        out_len=args.out_len,
    )
    print(output)
    print(tokenizer.untokenize(output))
