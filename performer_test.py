#!/usr/bin/env python3

import jax
from jax import random
import jax.numpy as jnp
import performer
from jax.nn.initializers import glorot_normal, normal


class TestPerformer:
    rng = random.PRNGKey(0)
    seq_len = 8
    encoding_len = 5
    num_features = 8
    dim_att = 7
    hidden_dim = 2
    input_shape = (seq_len, encoding_len)

    def test_apply_model(self):
        init_model, apply_model = performer.PerformerBlock(
            self.seq_len, self.num_features, self.dim_att, self.hidden_dim
        )
        key, rng = random.split(self.rng)
        output_shape, params, applys = init_model(key, self.input_shape)
        x = random.normal(key, self.input_shape)

        out = apply_model(params, applys, x)
        assert out.shape == self.input_shape

    def test_attention_equivalent_to_softmax(self):
        key, rng = random.split(self.rng)

        def SoftmaxAttention(
            dim_att,
            W_init=glorot_normal(),
            b_init=normal(),
        ):
            def init_fun(rng, input_shape):
                seq_len, encoding_len = input_shape
                k1, k2, k3, k4 = random.split(rng, 4)
                params_qkv = {
                    "weights": W_init(k1, (encoding_len, 3 * dim_att)),
                    "biases": b_init(k2, (3 * dim_att,)),
                }
                params_ffn = {
                    "weights": W_init(k3, (dim_att, encoding_len)),
                    "biases": b_init(k4, (encoding_len,)),
                }
                params = {"qkv": params_qkv, "ffn": params_ffn}
                return input_shape, params

            def apply_fun(params, x):
                seq_len, encoding_len = x.shape
                qkv = jnp.dot(x, params["qkv"]["weights"]) + params["qkv"]["biases"]
                q, k, v = jnp.split(qkv, 3, axis=-1)
                w = jnp.einsum("nd,md->nm", q, k)
                w = w / jnp.sqrt(dim_att)
                a = jax.nn.softmax(w, axis=-1)
                # unidirectional
                a = jnp.tril(a)
                x = jnp.einsum("nm,md->nd", a, v)
                out = jnp.dot(x, params["ffn"]["weights"]) + params["ffn"]["biases"]
                return out

            return init_fun, apply_fun

        init_sm_att, apply_sm_att = SoftmaxAttention(self.dim_att)
        key, rng = random.split(rng)
        _, params_sm_att = init_sm_att(key, self.input_shape)
        key, rng = random.split(rng)
        x = random.normal(key, self.input_shape)

        _, apply_perf_att = performer.PerformerAttention(
            self.num_features, self.dim_att
        )
        sm_out = apply_sm_att(params_sm_att, x)
        perf_att_out = apply_perf_att(params_sm_att, x)

        assert jnp.mean((sm_out - perf_att_out) ** 2) < 1e-1

    def test_favor_softmax_mse(self):
        key, rng = random.split(self.rng)
        qkv = random.normal(key, (self.seq_len, self.dim_att * 3))
        q, k, v = jnp.split(qkv, 3, axis=-1)

        def favor_prototype(q, k, v):
            key, rng = random.split(self.rng)
            projection = performer.orthogonal_gaussian(
                key, self.num_features, self.dim_att
            )
            # projection = random.normal(key, (self.num_features, self.dim_att))
            q_prime = performer.nonnegative_softmax_feature_map(q, projection)
            k_prime = performer.nonnegative_softmax_feature_map(k, projection)

            d_inv = jnp.diag(1 / (q_prime @ (k_prime.T @ jnp.ones(self.seq_len))))
            x = d_inv @ (q_prime @ (k_prime.T @ v))
            return x

        def sm_att(q, k, v, normalize=True):
            # https://github.com/teddykoker/performer/blob/main/performer.py
            l, d = q.shape
            normalizer = 1 / (d ** 0.5) if normalize else 1
            a = jnp.exp(q @ k.T * normalizer)
            d_inv = jnp.diag(1 / (a @ jnp.ones(l)))
            return d_inv @ a @ v

        assert jnp.mean((sm_att(q, k, v) - favor_prototype(q, k, v)) ** 2) < 1e-1
