import jax
from jax import random
import jax.numpy as jnp
from jax.nn.initializers import glorot_normal, normal, ones
import jaxlib
import functools


@functools.partial(jax.jit, static_argnums=(1,))
def layer_norm(
    x: jaxlib.xla_extension.DeviceArray,
    axis: int,
    eps: float = 1e-5,
) -> jaxlib.xla_extension.DeviceArray:
    """
    simple layer norm implementation
    ref: https://stackoverflow.com/questions/59830168/layer-normalization-in-pytorch

    x: input jax array
    axis: the axis along which you want layer norm to occur
    """
    # for axis = -1
    # x.shape = (seq_len, X)
    mean = jnp.mean(x, axis=axis, keepdims=True)
    # mean.shape = (seq_len, 1)
    var = jnp.var(x, axis=axis, keepdims=True)
    # var.shape = (seq_len, 1)
    out = (x - mean) / jnp.sqrt(var + eps)
    # out.shape = (seq_len, X)
    return out


def SpatialGatingUnit(W_init=glorot_normal(), b_init=ones):
    """
    ref: https://nn.labml.ai/transformers/gmlp/index.html
    """

    def init_fun(rng, input_shape):
        seq_len, dim_Z = input_shape
        output_shape = [seq_len, dim_Z // 2]
        k1, k2 = random.split(rng)
        params = {
            "weights": W_init(k1, (seq_len, seq_len)),
            "biases": b_init(k2, (seq_len,)),
        }
        return output_shape, params

    @jax.jit
    def apply_fun(params, inputs):
        # seq_len, dim_Z = inputs.shape

        # split inputs into z1 and z2
        z1, z2 = jnp.split(inputs, 2, axis=-1)

        # apply layer norm to z2
        z2 = layer_norm(z2, (-1,))

        # TODO verify that the biases are supposed to be added this way
        z2 = jnp.einsum("ij,jd->id", params["weights"], z2) + params["biases"][:, None]
        # element-wise multiplication
        out = z1 * z2
        return out

    return init_fun, apply_fun


def GMLPBlock(seq_len: int, d_Z: int, W_init=glorot_normal(), b_init=normal()):
    """
    layer constructor for a gMLP block
    """

    def init_fun(rng, input_shape):
        output_shape = input_shape
        seq_len, encoding_len = input_shape

        # proj1
        k1, k2, k3, k4 = random.split(rng, 4)
        params_proj1 = {
            "weights": W_init(k1, (encoding_len, d_Z)),
            "biases": b_init(k2, (d_Z,)),
        }

        # SGU
        init_sgu, apply_sgu = SpatialGatingUnit()
        _, params_sgu = init_sgu(rng, [seq_len, d_Z])

        # proj2
        params_proj2 = {
            "weights": W_init(k3, (d_Z // 2, encoding_len)),
            "biases": b_init(k4, (encoding_len,)),
        }

        params = {"proj1": params_proj1, "sgu": params_sgu, "proj2": params_proj2}
        applys = apply_sgu

        return output_shape, params, applys

    @functools.partial(jax.jit, static_argnums=(1,))
    def apply_fun(params, applys, x):
        params_proj1 = params["proj1"]
        params_sgu = params["sgu"]
        params_proj2 = params["proj2"]
        apply_sgu = applys

        shortcut = x
        # x.shape = (seq_len, encoding_len)
        x = layer_norm(x, (-1,))
        # x.shape = (seq_len, encoding_len)
        x = jnp.dot(x, params_proj1["weights"]) + params_proj1["biases"]
        # x.shape = (seq_len, d_Z)
        x = jax.nn.gelu(x)
        # x.shape = (seq_len, d_Z)
        x = apply_sgu(params_sgu, x)
        # x.shape = (seq_len, d_Z // 2)
        x = jnp.dot(x, params_proj2["weights"]) + params_proj2["biases"]
        # x.shape = (seq_len, encoding_len)
        return x + shortcut

    return init_fun, apply_fun


if __name__ == "__main__":
    key = random.PRNGKey(0)
    key, k1 = random.split(key)
    seq_len = 8
    encoding_len = 24
    d_Z = 10
    input_shape = jnp.array([seq_len, encoding_len])
    x = random.normal(k1, input_shape)

    init_model, apply_model = GMLPBlock(seq_len, d_Z)
    output_shape, params, applys = init_model(key, input_shape)
    out = apply_model(params, applys, x)
    print(out.shape)
