import jax
from jax import random
import jax.numpy as jnp
from jax.nn.initializers import glorot_normal, normal
import gmlp
import functools


def TinyAttention(d_Z, dim_att=64, W_init=glorot_normal(), b_init=normal()):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        k1, k2, k3, k4 = random.split(rng, 4)
        params_qkv = {
            "weights": W_init(k1, (encoding_len, 3 * dim_att)),
            "biases": b_init(k2, (3 * dim_att,)),
        }
        # the output of SGU is halved, and the output of the attention is
        # therefore halved too
        # ref: https://github.com/lucidrains/g-mlp-pytorch/blob/main/g_mlp_pytorch/g_mlp_pytorch.py
        params_ffn = {
            "weights": W_init(k3, (dim_att, d_Z // 2)),
            "biases": b_init(k2, (d_Z // 2,)),
        }
        output_shape = (seq_len, d_Z // 2)
        params = {"qkv": params_qkv, "ffn": params_ffn}
        return output_shape, params

    @jax.jit
    def apply_fun(params, x):
        seq_len, encoding_len = x.shape
        # x.shape = (seq_len, encoding_len)
        # params_att[0].shape = (encoding_len, 3 * dim_att)
        # this is a matrix multiplication with addition of bias
        qkv = jnp.dot(x, params["qkv"]["weights"]) + params["qkv"]["biases"]
        # qkv.shape = (seq_len, 3 * dim_att)
        # last axis is the one which is multiplied by three in init_fun
        # so that is the one we split on
        q, k, v = jnp.split(qkv, 3, axis=-1)
        # q.shape = (seq_len, dim_att)
        # k.shape = (seq_len, dim_att)
        # v.shape = (seq_len, dim_att)
        w = jnp.einsum("nd,md->nm", q, k)
        # w.shape = (seq_len, seq_len)
        # reduce weights magnitude
        # ref: https://arxiv.org/pdf/2105.08050.pdf
        w = w / jnp.sqrt(dim_att)
        # w.shape = (seq_len, seq_len)
        a = jax.nn.softmax(w, axis=-1)
        # a.shape = (seq_len, seq_len)
        x = jnp.einsum("nm,md->nd", a, v)
        # x.shape = (seq_len, dim_att)
        # see the paper and https://github.com/lucidrains/g-mlp-pytorch/blob/970966c10c10770b2098b8f88c7d1f86efbf611b/g_mlp_pytorch/g_mlp_pytorch.py#L96
        # params_fnn[0].shape = (dim_att, d_Z // 2)
        out = jnp.dot(x, params["ffn"]["weights"]) + params["ffn"]["biases"]
        # out.shape = (seq_len, d_Z // 2)
        return out

    return init_fun, apply_fun


def AMLPBlock(seq_len: int, d_Z: int, W_init=glorot_normal(), b_init=normal()):
    """
    layer constructor for a gMLP block
    """

    def init_fun(rng, input_shape):
        output_shape = input_shape
        seq_len, encoding_len = input_shape

        # attention
        init_att, apply_att = TinyAttention(d_Z)
        _, params_att = init_att(rng, input_shape)

        # proj1
        k1, k2, k3, k4 = random.split(rng, 4)
        params_proj1 = {
            "weights": W_init(k1, (encoding_len, d_Z)),
            "biases": b_init(k2, (d_Z,)),
        }

        # SGU
        init_sgu, apply_sgu = gmlp.SpatialGatingUnit()
        _, params_sgu = init_sgu(rng, [seq_len, d_Z])

        # proj2
        W2, b2 = W_init(k3, (d_Z // 2, encoding_len)), b_init(k4, (encoding_len,))
        params_proj2 = {
            "weights": W_init(k3, (d_Z // 2, encoding_len)),
            "biases": b_init(k4, (encoding_len,)),
        }

        params = {
            "att": params_att,
            "proj1": params_proj1,
            "sgu": params_sgu,
            "proj2": params_proj2,
        }
        applys = (apply_att, apply_sgu)

        return output_shape, params, applys

    @functools.partial(jax.jit, static_argnums=(1,))
    def apply_fun(params, applys, x):
        apply_att, apply_sgu = applys

        shortcut = x
        x = gmlp.layer_norm(x, (-1,))
        # x.shape = (seq_len, encoding_len)
        att = apply_att(params["att"], x)
        # att.shape = (seq_len, d_Z // 2)
        # params_proj1[0].shape = (encoding_len, d_Z)
        x = jnp.dot(x, params["proj1"]["weights"]) + params["proj1"]["biases"]
        # x.shape = (seq_len, d_Z)
        x = jax.nn.gelu(x)
        # x.shape = (seq_len, d_Z)
        x = apply_sgu(params["sgu"], x)
        # x.shape = (seq_len, d_Z // 2)
        x = x + att
        # x.shape = (seq_len, d_Z // 2)
        # params_proj2[0].shape = (d_Z // 2, encoding_len)
        x = jnp.dot(x, params["proj2"]["weights"]) + params["proj2"]["biases"]
        # x.shape = (seq_len, encoding_len)
        out = x + shortcut
        # out.shape = (seq_len, encoding_len)
        return out

    return init_fun, apply_fun


if __name__ == "__main__":
    key = random.PRNGKey(0)
    key, k1 = random.split(key)
    seq_len = 8
    encoding_len = 5
    d_Z = 20
    input_shape = jnp.array([seq_len, encoding_len])
    x = random.normal(k1, input_shape)

    init_model, apply_model = AMLPBlock(seq_len, d_Z)
    output_shape, params, applys = init_model(key, input_shape)
    out = apply_model(params, applys, x)
    print(out.shape)
