#!/usr/bin/env python3

import jax
from jax import random
import jax.numpy as jnp
from jax.nn.initializers import glorot_normal, normal
import prefix_sum
import functools
import einops


@jax.jit
def RoPE(x: jnp.DeviceArray, base: int = 10_000):
    """
    Apply rotary positional embeddings to the input
    reference:
    https://github.com/kingoflolz/mesh-transformer-jax/blob/9d15698694a2a54ff0a32ec5ae4ec05e7a388bee/mesh_transformer/layers.py#L146
    """
    seq_len, encoding_len = x.shape
    theta = 1.0 / (base ** (jnp.arange(0, encoding_len, 2) / encoding_len))
    seq_arange_theta = jnp.einsum("i,j->ij", jnp.arange(seq_len), theta)
    sin = einops.repeat(
        jnp.sin(seq_arange_theta),
        "seq_arange theta -> seq_arange (theta repeat)",
        repeat=2,
    )
    cos = einops.repeat(
        jnp.cos(seq_arange_theta),
        "seq_arange theta -> seq_arange (theta repeat)",
        repeat=2,
    )

    even_embeds = x[:, ::2]
    odd_embeds = x[:, 1::2]
    stacked = jnp.stack((-odd_embeds, even_embeds), axis=-1)
    rotated_every_second_element = einops.rearrange(
        stacked, "seq_len encoding_len r -> seq_len (encoding_len r)"
    )
    return (x * cos) + (rotated_every_second_element * sin)


@functools.partial(jax.jit, static_argnums=(1,))
def layer_norm(
    x: jnp.DeviceArray,
    axis: int,
    eps: float = 1e-5,
) -> jnp.DeviceArray:
    """
    simple layer norm implementation
    ref: https://stackoverflow.com/questions/59830168/layer-normalization-in-pytorch

    x: input jax array
    axis: the axis along which you want layer norm to occur
    """
    # for axis = -1
    # x.shape = (seq_len, X)
    mean = jnp.mean(x, axis=axis, keepdims=True)
    # mean.shape = (seq_len, 1)
    var = jnp.var(x, axis=axis, keepdims=True)
    # var.shape = (seq_len, 1)
    out = (x - mean) / jnp.sqrt(var + eps)
    # out.shape = (seq_len, X)
    return out


# @functools.partial(jax.jit, static_argnums=(0,))
def orthogonal_square(rng, dim):
    key, rng = random.split(rng)
    unstructured_block = random.normal(key, (dim, dim))
    q, _ = jnp.linalg.qr(unstructured_block)
    return q.T


# @jax.jit
def orthogonal_gaussian(rng, num_features, num_columns):
    num_squares = int(num_features / num_columns)
    blocks = []
    for _ in range(num_squares):
        key, rng = random.split(rng)
        blocks.append(orthogonal_square(key, num_columns))
    remaining_features = num_features - num_squares * num_columns
    if remaining_features:
        key, rng = random.split(rng)
        blocks.append(orthogonal_square(key, num_columns)[:remaining_features])
    final_matrix = jnp.vstack(blocks)
    # attention scaling
    # empirically better than koker's implementation of scaling
    multiplier = jnp.sqrt(num_columns) * jnp.ones((num_features))
    return jnp.diag(multiplier) @ final_matrix


@jax.jit
def nonnegative_softmax_feature_map(data, projection, eps=1e-5):
    seq_len, dim_att = data.shape
    num_features, dim_att = projection.shape
    # construct feature map input normalizer
    normalizer = dim_att ** -0.25

    log_multiplier = (
        -jnp.square(data * (normalizer ** 2)).sum(axis=-1, keepdims=True) / 2
    )
    log_features = (data * normalizer) @ projection.T
    return jnp.exp(log_multiplier + log_features + eps)


def PerformerAttention(
    num_features,
    dim_att,
    W_init=glorot_normal(),
    b_init=normal(),
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        k1, k2, k3, k4 = random.split(rng, 4)
        params_qkv = {
            "weights": W_init(k1, (encoding_len, 3 * dim_att)),
            "biases": b_init(k2, (3 * dim_att,)),
        }
        params_ffn = {
            "weights": W_init(k3, (dim_att, encoding_len)),
            "biases": b_init(k4, (encoding_len,)),
        }
        params = {"qkv": params_qkv, "ffn": params_ffn}
        return input_shape, params

    @jax.jit
    def apply_fun(params, x):
        seq_len, encoding_len = x.shape
        qkv = jnp.dot(x, params["qkv"]["weights"]) + params["qkv"]["biases"]
        # qkv.shape = (seq_len, 3 * dim_att)
        q, k, v = jnp.split(qkv, 3, axis=-1)
        # {q, k, v}.shape = (seq_len, dim_att)
        dim_att = q.shape[1]
        # construct projection tensor
        query_seed = jax.lax.convert_element_type(
            jnp.ceil(jnp.sum(q) * 10000000.0), jnp.int32
        )
        rng = random.PRNGKey(query_seed)
        projection = orthogonal_gaussian(rng, num_features, dim_att)
        # projection = random.normal(rng, (num_features, dim_att))
        q_prime = nonnegative_softmax_feature_map(q, projection)
        # q_prime.shape = (seq_len, num_features)
        k_prime = nonnegative_softmax_feature_map(k, projection)
        # k_prime.shape = (seq_len, num_features)

        # TODO verify the prefix sum functions and replace / minify them if
        # necessary
        # unidirectional attention
        # for lax.scan init_value
        z_slice_shape = (k_prime.shape[-1], v.shape[-1])
        numerator_fun = prefix_sum._numerator(z_slice_shape)
        W = numerator_fun(q_prime, k_prime, v)
        # W.shape = (seq_len, dim_att)

        # normalize unidirectional attention
        denominator_fun = prefix_sum._denominator(k_prime.shape[-1])
        R = denominator_fun(q_prime, k_prime)
        # R.shape = (seq_len,)
        numeric_stabilizer = 1e-6
        R = R + 2 * numeric_stabilizer * (jnp.abs(R) <= numeric_stabilizer)
        R = jnp.reciprocal(R)
        R = jnp.expand_dims(R, len(R.shape))
        # R.shape = (seq_len, 1)
        x = W * R
        # x.shape = (seq_len, dim_att)
        # params["ffn"]["weights"].shape = (dim_att, encoding_len)
        out = jnp.dot(x, params["ffn"]["weights"]) + params["ffn"]["biases"]
        # out.shape = (seq_len, encoding_len)
        return out

    return init_fun, apply_fun


def PerformerBlock(
    seq_len: int,
    num_features: int,
    dim_att: int,
    hidden_dim: int,
    W_init=glorot_normal(),
    b_init=normal(),
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape

        # attention
        init_att, apply_att = PerformerAttention(num_features, dim_att)
        key, rng = random.split(rng)
        _, params_att = init_att(key, input_shape)

        k1, k2 = random.split(rng)
        params_proj = {
            "weights": W_init(k1, (encoding_len, hidden_dim)),
            "biases": b_init(k2, (hidden_dim,)),
        }
        params_proj2 = {
            "weights": W_init(k1, (hidden_dim, encoding_len)),
            "biases": b_init(k2, (encoding_len,)),
        }
        params = {
            "att": params_att,
            "proj": params_proj,
            "proj2": params_proj2,
        }
        applys = apply_att
        return input_shape, params, applys

    @functools.partial(jax.jit, static_argnums=(1,))
    def apply_fun(params, applys, x):
        # based on GPT-2 architecture
        apply_att = applys

        skip = x
        x = layer_norm(x, (-1,))
        x = RoPE(x)
        x = apply_att(params["att"], x)
        x = x + skip

        skip = x
        x = layer_norm(x, (-1,))
        x = jnp.dot(x, params["proj"]["weights"] + params["proj"]["biases"])
        x = jax.nn.gelu(x)
        x = jnp.dot(x, params["proj2"]["weights"] + params["proj2"]["biases"])
        x = x + skip
        return x

    return init_fun, apply_fun


def PerformerStack(
    dim_att: int,
    stack_len: int,
    hidden_dim: int,
    num_features: int,
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        list_params = []
        list_applys = []
        for i in range(stack_len):
            init_block, apply_block = PerformerBlock(
                seq_len, num_features, dim_att, hidden_dim
            )
            key, rng = random.split(rng)
            input_shape, params, applys = init_block(key, input_shape)
            list_params.append(params)
            list_applys.append((applys, apply_block))
        params = {"performer_block": list_params}
        return input_shape, params, tuple(list_applys)

    def apply_fun(params, list_applys, x):
        for i in range(stack_len):
            applys, apply_block = list_applys[i]
            x = apply_block(params["performer_block"][i], applys, x)
        return x

    return init_fun, apply_fun
