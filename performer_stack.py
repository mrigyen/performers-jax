#!/usr/bin/env python3

from performer import PerformerBlock
from jax import random
import jax_tokenizer as tokenizer


def PerformerStack(
    seq_len: int,
    num_features: int,
    dim_att: int,
    stack_len: int,
    hidden_dim: int,
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        list_params = []
        list_applys = []
        for i in range(stack_len):
            init_block, apply_block = PerformerBlock(
                seq_len, num_features, dim_att, hidden_dim
            )
            key, rng = random.split(rng)
            input_shape, params, applys = init_block(key, input_shape)
            list_params.append(params)
            list_applys.append((applys, apply_block))
        params = {"performer_block": list_params}
        return input_shape, params, tuple(list_applys)

    def apply_fun(params, list_applys, x):
        for i in range(stack_len):
            applys, apply_block = list_applys[i]
            x = apply_block(params["performer_block"][i], applys, x)
        return x

    return init_fun, apply_fun


if __name__ == "__main__":
    rng = random.PRNGKey(0)
    seq_len = 64
    num_features = 3
    dim_att = 64
    stack_len = 3
    hidden_dim = 4
    x = tokenizer.pad_right(tokenizer.tokenize("Hello world!"), seq_len)
    print(x.shape)
    init_s, apply_s = PerformerStack(
        seq_len, num_features, dim_att, stack_len, hidden_dim
    )

    key, rng = random.split(rng)
    _, list_params, list_applys = init_s(key, (seq_len, 256))
    out = apply_s(list_params, list_applys, tokenizer.magnify(x))
    print(out.shape)
    real_out = tokenizer.untokenize(tokenizer.reduce(out))
    print(real_out)
