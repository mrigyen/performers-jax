#!/usr/bin/env bash

export CLOUDSDK_PYTHON=/usr/bin/python2
NUM_DEVICES=$1

gcloud alpha compute tpus tpu-vm \
    scp scripts/dataset/get_pile_val.sh papuahardynet-big: \
  --worker=all --zone=europe-west4-a

for i in $(seq 0 $(echo $NUM_DEVICES - 1 | bc))
do
    gcloud alpha compute tpus tpu-vm ssh papuahardynet-big \
        --zone europe-west4-a \
        --worker=$i \
        --command "sh get_pile_val.sh $i $NUM_DEVICES"
done
