#!/usr/bin/env bash
sudo apt update
# to unzstd the pile dataset files
sudo apt install zstd
pip install --upgrade pip
pip install "jax[tpu]>=0.2.16" -f https://storage.googleapis.com/jax-releases/libtpu_releases.html
pip install jsonlines
pip install numba
pip install wandb
