#!/usr/bin/env bash
# train an aMLP model that has parameters similar to GPT-2 small (124M)
# sidenote: the GPT-2 paper uses the size 117M, but the now accepted size for
# GPT-2 small is 124M
# % python3 get_model_size.py --seq_len 3072 --stack_len 13 --hidden_dim 768
#   --architecture "aMLP"
# 127.536448M
STACK_LEN=13
HIDDEN_DIM=768
SEQ_LEN=3072
BATCH_SIZE=4
MODEL_NAME="a127M"
DATASET_DIR="$HOME/dataset/"
ARCHITECTURE="aMLP"
SEED=0
mkdir -p checkpoints/

if $(sudo lsof -w /dev/accel0); then
	echo "TPU device already in use by a process"
else
	# train the model
	python3 train.py \
		--root_dir "$DATASET_DIR" \
		--seq_len $SEQ_LEN \
		--stack_len $STACK_LEN \
		--hidden_dim $HIDDEN_DIM \
		--sub_batch_size $BATCH_SIZE \
		--save_path checkpoints/$MODEL_NAME.pkl \
		--architecture $ARCHITECTURE \
		--seed $SEED
fi
